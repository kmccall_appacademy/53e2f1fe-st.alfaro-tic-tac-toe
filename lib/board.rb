class Board
  attr_reader :grid, :winner, :grid_size, :row_feas, :col_feas, :diag_feas
  def initialize(grid = nil, grid_size = 3)
    @grid_size = grid_size
    if grid.nil?
      @grid = Array.new(grid_size) { Array.new(grid_size) }
    else
      @grid = grid
    end
    @winner = nil
    @row_feas = Array.new(@grid_size) { true }
    @col_feas = Array.new(@grid_size) { true }
    @diag_feas = Array.new(2) { true }
  end

  def place_mark(pos, mark)
    if empty?(pos)
      self[pos] = mark
      @winner = mark if win?(pos, mark)
    end
  end

  def empty?(pos)
    self[pos].nil?
  end

  def over?
    if winner || tie?
      true
    else
      false
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def win?(pos, mark)
    return true if horizontal_win?(pos, mark)
    return true if vertical_win?(pos, mark)
    return true if diagonal_win?(mark)
    false
  end

  def display
    (0...grid_size).each do |row|
      line_output = ""
      (0...grid_size).each do |col|
        line_output << "[#{self[[row, col]]}]"
      end
      puts line_output
    end
  end

  private

  def tie?
    all_false?(@row_feas) && all_false?(@col_feas) && all_false?(@diag_feas)
  end

  def all_false?(arr)
    arr.all? { |element|  element == false}
  end

  def board_empty?
    (0...@grid_size).each do |row|
      (0...@grid_size).each do |col|
        return false unless self[[row, col]].nil?
      end
    end
    true
  end

  def horizontal_win?(pos, mark)
    row = pos[0]
    (0...@grid_size).each do |col|
      next if pos == [row, col]
      unless self[[row, col]] == mark
        @row_feas[row] = false if self[[row, col]]
        return false
      end
    end
    true
  end

  def vertical_win?(pos, mark)
    col = pos[1]
    (0...@grid_size).each do |row|
      next if pos == [row, col]
      unless self[[row, col]] == mark
        @col_feas[col] = false if self[[row, col]]
        return false
      end
    end
    true
  end

  def diagonal_win?(mark)
    left_diag = true
    right_diag = true
    (0...@grid_size).each do |idx|
      unless self[[idx, idx]] == mark
        @diag_feas[0] = false if self[[idx, idx]]
        left_diag = false
      end
      unless self[[idx, @grid_size - 1 - idx]] == mark
        @diag_feas[1] = false if self[[idx, @grid_size - 1 - idx]]
        right_diag = false
      end
    end
    left_diag || right_diag
  end
end
