require_relative 'player'

class ComputerPlayer < Player
  attr_reader :board
  attr_accessor :mark

  def initialize(name, mark = nil)
    super(name, mark)
  end

  def display(board)
    @board = board
  end

  def get_move
    first_empty_slot = nil
    (0...board.grid_size).each do |row|
      next unless board.row_feas[row]
      (0...board.grid_size).each do |col|
        next unless board.col_feas[col]
        first_empty_slot ||= [row, col]
        return [row, col] if board.win?([row, col], mark)
      end
    end
    first_empty_slot
  end

end
