require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player
  def initialize(p1, p2)
    @p1 = p1
    @p2 = p2
    @board = Board.new
    @current_player = p1
  end

  def play_turn
    board.place_mark(@current_player.get_move, @current_player.mark)
    board.display
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
  end

  def switch_players!
    @current_player = @current_player == @p1 ? @p2 : @p1
  end

end
