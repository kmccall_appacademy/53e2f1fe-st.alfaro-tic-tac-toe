require_relative 'player'

class HumanPlayer < Player
  def initialize(name, mark = nil)
    super(name, mark)
  end

  def get_move
    puts "where?"
    gets.chomp.split(",").map(&:to_i)
  end

end
