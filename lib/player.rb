class Player
  attr_accessor :mark
  attr_reader :name
  def initialize(name, mark = nil)
    @name = name
    @mark = mark
  end

  def get_move; end

  def display(board)
    board.display
  end
end
